<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' =>'auth'], function()
{
    Route::get('/', function () {
        return view('admin.blank_page.index');
    });

    Route::resource('categories', '\App\Http\Controllers\CategoryController');
    Route::get('products/export-pdf', '\App\Http\Controllers\ProductController@exportPdf')->name('products.export_pdf');
    Route::get('products/export-excel', '\App\Http\Controllers\ProductController@exportExcel')->name('products.export_excel');

    Route::resource('products', '\App\Http\Controllers\ProductController');
});
Auth::routes();
