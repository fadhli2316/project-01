@extends('admin.layouts.app')

@section('content')

{{-- *Content Wrapper --}}
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Tambah Data Barang</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Tambah Data Barang</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <a href="{{ route('products.index') }}" class="btn btn-info">Kembali</a>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="{{ route('products.store') }}">
          @csrf
          {{-- name --}}
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror"  placeholder="nama" name="name" value="{{ old('name') }}">
              @error('name')
              <div class="invalid-feedback">{{ $errors->first('name') }}</div>
              @enderror
            </div>

            {{-- kategori --}}
             <div class="form-group">
                <label for="exampleInputEmail1">kategori barang</label>
                <select name="category_id"  class="form-control @error('category_id') is-invalid @enderror"  >
                    <option value="">[ Pilih Kategori : ]</option>
                    @foreach($categories as $category)

                        <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? "selected" : "" }}> {{ $category->name }} </option>

                    @endforeach
                </select>
                @error('name')

                <div class="invalid-feedback">{{ $errors->first('stock') }}</div>

                @enderror
            </div>

            {{-- STOCK --}}
            <div class="form-group">
                <label for="exampleInputEmail1">Stock</label>
                <input type="number" class="form-control @error('stock') is-invalid @enderror"  placeholder="stock" name="stock" value="{{ old('stock') }}">
                @error('stock')

                <div class="invalid-feedback">{{ $errors->first('stock') }}</div>

                @enderror
            </div>

                   {{-- harga --}}
                <div class="form-group">
                    <label for="exampleInputEmail1">price</label>
                    <input type="text" class="form-control @error('price') is-invalid @enderror"  placeholder="price" name="price" value="{{ old('price') }}">
                    @error('price')

                    <div class="invalid-feedback">{{ $errors->first('price') }}</div>

                    @enderror
                </div>

            {{-- image --}}
            <div class="form-group">
                <label for="exampleInputEmail1">image</label>
                <input type="text" class="form-control @error('image') is-invalid @enderror"  placeholder="image" name="image" value="{{ old('image') }}">

            </div>

            {{-- description --}}
            <div class="form-group">
                <label for="exampleInputEmail1">description</label>
                <textarea name="description" id="" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror"> </textarea>

                @error('name')
                <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                @enderror
            </div>

          </div>

          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
{{-- *End-content-wrapper --}}

@endsection
