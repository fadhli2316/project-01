@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Data Barang</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Data Barang</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('products.index') }}" class="btn btn-success">Kembali</a>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <form method="POST" action="{{ route('products.update', $product->id) }}">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text"
                                class="form-control @error('name') is-invalid
                            @enderror"
                                placeholder="Nama" name="name" value="{{ old('name', $product->name) }}">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Kategori Barang</label>
                            <select name="category_id" class="form-control @error('category_id') is-invalid @enderror">
                                <option value="">[ Pilih Kategori ]</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ $errors->any() ? (old('category_id') == $category->id ? 'selected' : '') : ($product->category_id == $category->id ? 'selected' : '') }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('category_id')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Stok</label>
                            <input type="number"
                                class="form-control @error('stock') is-invalid
                            @enderror"
                                placeholder="Stock" name="stock" value="{{ old('stock', $product->stock) }}">
                            @error('stock')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Gambar</label>
                            <input type="text"
                                class="form-control @error('image') is-invalid
                            @enderror"
                                placeholder="image" name="image" value="{{ old('image', $product->image) }}">
                            @error('image')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Harga</label>
                            <input type="number"
                                class="form-control @error('price') is-invalid
                            @enderror"
                                placeholder="price" name="price" value="{{ old('price', $product->price) }}">
                            @error('price')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        {{-- ! Status --}}
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" cols="30" rows="10"
                                class="form-control @error('description') is-invalid @enderror">{{ old('description', $product->description) }}
                            </textarea>
                            @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
