@extends('admin.layouts.app')

@section('content')

{{-- *Content Wrapper --}}
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Barang</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <a href="{{ route('products.create') }}" class="btn btn-primary">Tambah Data</a>
        <a href="{{ route('products.export_pdf') }}" target=*_blank* class="btn btn-info">Export PDF</a>
        <a href="{{ route('products.export_excel') }}" target=*_blank* class="btn btn-light border border-dark">Export excel</a>



        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Nama Kategori</th>
              <th>Stok</th>
              <th>Harga</th>
              <th>Deskrips</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($products as $index => $product)
              <tr>
                <td>{{ $index + 1 }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->description }}</td>
                <td class="d-flex justify-content-start ">
                  <a href="{{ route('products.edit',$product->id) }}" class="btn btn-info btn-sm ">EDIT</a>
                  <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    @csrf
                    {{ method_field('DELETE') }}
                    <button class="btn-danger btn btn-sm mx-2" type="submit"> Hapus </button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
{{-- *End-content-wrapper --}}

@endsection
