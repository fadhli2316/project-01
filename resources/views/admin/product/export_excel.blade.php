<table border="1" width="100%" style="border-collapse::collapse;">
    <tr>
        <th> No </th>
        <th> Nama </th>
        <th> Stock </th>
        <th> Harga </th>
        <th> Deskripsi </th>
    </tr>
    @foreach ( $products as $index => $product )
        <tr>
            <td> {{ $index + 1 }} </td>
            <td> {{ $product->name }} </td>
            <td> {{ $product->stock }} </td>
            <td> {{ $product->price }} </td>
            <td> {{ $product->description }} </td>
        </tr>
    @endforeach
    {{-- {{ dd($products) }} --}}

</table>
